import urllib2
from xml.sax import parseString
from Handlers import GenerelHandler
from Handlers import OversigtsHandler
from Handlers import ResultatHandler


def get_partier(valg_id):
    try:
        response = urllib2.urlopen('http://www.dst.dk/valg/' + valg_id + '/xml/generel.xml')
        generelData = response.read()
    except Exception as e:
        print(str(e.message))
        return None

    generelHandler = GenerelHandler()
    parseString(generelData, generelHandler)

    return generelHandler.partier


def get_oversigts_struktur(url):
    try:
        response = urllib2.urlopen(url)
        rawData = response.read()
    except Exception as e:
        print(str(e.message))
        return None

    oversigtsHandler = OversigtsHandler()
    parseString(rawData, oversigtsHandler)
    return oversigtsHandler


def update(omraade):
    try:
        response = urllib2.urlopen(omraade.filnavn)
        resultatData = response.read()
    except Exception as e:
        print(str(e.message))

    resultatHandler = ResultatHandler()
    parseString(resultatData, resultatHandler)
    omraade.resultat = resultatHandler.resultat