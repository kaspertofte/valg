import time
from DSTQueries import get_partier
from DSTQueries import get_oversigts_struktur
from DSTQueries import update

from Diverse import get_parti_ID
from Diverse import countPersonlige

from GoogleSheetsAPI import ResultSpreadSheet

if __name__ == '__main__':
    VALG_ID = 'Valg1684447'  # Juni 2019
    #VALG_ID = 'Valg1684426'  # EP 2019
    #VALG_ID = 'Valg1487635'  # Juni 2015
    TYPE = 'VALGDAG'
    TYPE = 'FINTAL'

    oversigtsURL = ""
    if TYPE == 'VALGDAG':
        oversigtsURL = 'http://www.dst.dk/valg/' + VALG_ID + '/xml/valgdag.xml'
    elif TYPE == 'FINTAL':
        oversigtsURL = 'http://www.dst.dk/valg/' + VALG_ID + '/xml/fintal.xml'
    else:
        exit(1)

    partier = get_partier(VALG_ID)
    while partier is None:
        partier = get_partier(VALG_ID)

    print('Getting new data')
    omraader = get_oversigts_struktur(oversigtsURL)
    while omraader is None:
        omraader = get_oversigts_struktur(oversigtsURL)

    sheet = ResultSpreadSheet('1yG-AXtLlK0H4fjVCmYUOW1FBsh034YXNzk2HyrVYq_I', ['https://www.googleapis.com/auth/spreadsheets'])
    sheet.logon()

    sheet.make_initial_setup(omraader.storKredse, omraader.opstillingsKredse, omraader.afstemningsomraader)

    idx = 0
    for id in omraader.alle:
        if omraader.alle[id].senestRettet != '0001-01-01T00:00:00':
            print(omraader.alle[id].navn + ": " + omraader.alle[id].filnavn)
            update(omraader.alle[id])
            sheet.update_omraade(omraader.alle[id].resultat, omraader.alle, partier)
            idx = idx + 1
            q, r = divmod(idx, 50)
            if r == 0:
                time.sleep(100)
                idx = 0

    while True:
        print('Getting new data')
        nyData = get_oversigts_struktur(oversigtsURL)
        while nyData is None:
            nyData = get_oversigts_struktur(oversigtsURL)

        idx = 0
        for id in nyData.alle:
            if omraader.alle[id].senestRettet < nyData.alle[id].senestRettet:
                print(omraader.alle[id].navn + ": " + omraader.alle[id].filnavn)
                update(omraader.alle[id])
                if sheet.update_omraade(omraader.alle[id].resultat, omraader.alle, partier):
                    omraader.alle[id].senestRettet = nyData.alle[id].senestRettet

                idx = idx + 1
                q, r = divmod(idx, 50)
                if r == 0:
                    time.sleep(100)
                    idx = 0

        time.sleep(180)
