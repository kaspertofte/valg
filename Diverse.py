def countPersonlige(omraader, partiID):
    resultat = {}
    for omraade in omraader:
        if partiID in omraader[omraade].resultat.personResultat:
            personer = omraader[omraade].resultat.personResultat[partiID]
            for person in personer:
                if person.navn in resultat:
                    resultat[person.navn] += person.personligeStemmer
                else:
                    resultat[person.navn] = person.personligeStemmer
        else:
            print omraader[omraade].navn + " " + omraader[omraade].filnavn
    return resultat


def get_parti_ID(bogstav, partier):
    for parti in partier:
        if partier[parti].bogstav == bogstav:
            return partier[parti].id
    return ''


def get_column(col):
    """ Convert given row and column number to an Excel-style cell name. """
    quot, rem = divmod(col-1, 26)
    return((chr(quot-1 + ord('A')) if quot else '') +
           (chr(rem + ord('A')) ))


def get_storkreds_id(id, omraader):
    if omraader[id].omraadeType == 'Landsdel':
        return None
    elif omraader[id].omraadeType =="Storkreds":
        return id
    else:
        return get_storkreds_id(omraader[id].parentID, omraader)

