class Parti():
    def __init__(self, id, bogstav, navn):
        self.id = id
        self.bogstav = bogstav
        self.navn = navn

    def to_string(self):
        return self.bogstav + " " + self.navn + " (" + self.id + ")"


class PersonResultat():
    def __init__(self, navn, personligeStemmer, tildelteStemmer, partiID):
        self.navn = navn
        self.personligeStemmer = personligeStemmer
        self.tildelteStemmer = tildelteStemmer
        self.partiID = partiID

    def stemmer(self):
        return self.personligeStemmer + self.tildelteStemmer


class OmraadeResultat():
    def __init__(self):
        self.personResultat = {} # (parti_id : [ PersonResultat ] )
        self.stemmer = {} # (parti_id : stemmer)
        self.udenforParti = 0
        self.udenforPartiPct = 0
        self.ialtGyldige = 0
        self.blanke = 0
        self.andreUgyldige = 0
        self.stemmeberettigede = 0
        self.omraadeID = 0


class OpstillingsOmraade():
    def __init__(self, senestRettet, id, parentID, filnavn, omraadeType):
        self.senestRettet = senestRettet
        self.id = id
        self.parentID = parentID
        self.filnavn = filnavn
        self.navn = ""
        self.resultat = None
        self.omraadeType = omraadeType

    def to_string(self):
        return self.navn + " " + self.id + " " + self.senestRettet + " " + self.filnavn
