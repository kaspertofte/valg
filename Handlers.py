from xml.sax import ContentHandler
from DataObjects import OpstillingsOmraade
from DataObjects import Parti
from DataObjects import OmraadeResultat
from DataObjects import PersonResultat


class OversigtsHandler(ContentHandler):
    def __init__(self):
        ContentHandler.__init__(self)
        self.currentData = ""
        self.currentObject = None
        self.landsDele = {}
        self.storKredse = {}
        self.opstillingsKredse = {}
        self.afstemningsomraader = {}
        self.alle = {}

    def alle_omraader(self):
        result = {}
        result.update(self.landsDele)
        result.update(self.storKredse)
        result.update(self.opstillingsKredse)
        result.update(self.afstemningsomraader)
        return result

    # Call when an element starts
    def startElement(self, tag, attributes):
        self.currentData = tag
        if tag == "Landsdel":
            self.currentObject = OpstillingsOmraade(attributes['SenestRettetIso'], attributes['landsdel_id'], attributes['land_id'], attributes['filnavn'], 'Landsdel')
            self.landsDele[attributes['landsdel_id']] = self.currentObject
            self.alle[attributes['landsdel_id']] = self.currentObject
        elif tag == "Storkreds":
            self.currentObject = OpstillingsOmraade(attributes['SenestRettetIso'], attributes['storkreds_id'],
                                                    attributes['landsdel_id'], attributes['filnavn'], 'Storkreds')
            self.storKredse[attributes['storkreds_id']] = self.currentObject
            self.alle[attributes['storkreds_id']] = self.currentObject
        elif tag == "Opstillingskreds":
            self.currentObject = OpstillingsOmraade(attributes['SenestRettetIso'], attributes['opstillingskreds_id'],
                                                    attributes['storkreds_id'], attributes['filnavn'], 'Opstillingskreds')
            self.opstillingsKredse[attributes['opstillingskreds_id']] = self.currentObject
            self.alle[attributes['opstillingskreds_id']] = self.currentObject
        elif tag == "Afstemningsomraade":
            self.currentObject = OpstillingsOmraade(attributes['SenestRettetIso'], attributes['afstemningsomraade_id'],
                                                    attributes['opstillingskreds_id'], attributes['filnavn'], 'Afstemningsomraade')
            self.afstemningsomraader[attributes['afstemningsomraade_id']] = self.currentObject
            self.alle[attributes['afstemningsomraade_id']] = self.currentObject

    # Call when an elements ends
    def endElement(self, tag):
        self.currentData = ""
        self.currentObject = None

    # Call when a character is read
    def characters(self, content):
        if self.currentObject is not None:
            self.currentObject.navn = content


class GenerelHandler(ContentHandler):
    def __init__(self):
        ContentHandler.__init__(self)
        self.isOpstilledePartier = False
        self.isUdenForParti = False
        self.partier = {}

    # Call when an element starts
    def startElement(self, tag, attributes):
        if tag == "Parti":
            if self.isUdenForParti:
                self.partier[attributes['Id']] = Parti(attributes['Id'], "", attributes['Navn'])
            if self.isOpstilledePartier:
                self.partier[attributes['Id']] = Parti(attributes['Id'], attributes['Bogstav'], attributes['Navn'])

        if tag == "OpstilledePartier":
            self.isOpstilledePartier = True
        if tag == "KandidaterUdenForPartier":
            self.isUdenForParti = True

    # Call when an elements ends
    def endElement(self, tag):
        if tag == "OpstilledePartier":
            self.isOpstilledePartier = False
        if tag == "KandidaterUdenForPartier":
            self.isUdenForParti = False


class ResultatHandler(ContentHandler):
    def __init__(self):
        ContentHandler.__init__(self)
        self.currentData = ""
        self.currentParti = ""
        self.resultat = OmraadeResultat()
        self.inStemmer = False
        self.inPersoner = False

    # Call when an element starts
    def startElement(self, tag, attributes):
        self.currentData = tag
        if tag == 'Stemmer':
            self.inStemmer = True
        elif tag == 'Personer':
            self.inPersoner = True
        elif tag == 'Parti':
            self.currentParti = attributes['Id']
            if self.inStemmer:
                self.resultat.stemmer[attributes['Id']] = int(attributes['StemmerAntal'])
        elif tag == 'Person':
            person = PersonResultat(attributes['Navn'], int(attributes['PersonligeStemmer']), int(attributes.get('TildeltePartiStemmer', 0)), self.currentParti)
            if self.currentParti in self.resultat.personResultat:
                self.resultat.personResultat[self.currentParti].append(person)
            else:
                self.resultat.personResultat[self.currentParti] = [person]
        elif tag == 'Sted':
            self.resultat.omraadeID = attributes['Id']

    # Call when an elements ends
    def endElement(self, tag):
        self.currentData = ""
        if tag == 'Stemmer':
            self.inStemmer = False
        elif tag == 'Personer':
            self.inPersoner = False
        elif tag == 'Parti':
            self.currentParti = ''

    # Call when a character is read
    def characters(self, content):
        if self.currentData == "Stemmeberettigede":
            self.resultat.stemmeberettigede = int(content)
        elif self.currentData == 'IAltGyldigeStemmer':
            self.resultat.ialtGyldige = int(content)
        elif self.currentData == 'BlankeStemmer':
            self.resultat.blanke = int(content)
        elif self.currentData == 'AndreUgyldigeStemmer':
            self.resultat.andreUgyldige = int(content)
