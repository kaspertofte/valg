from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.errors import HttpError

from Diverse import get_column
from Diverse import get_storkreds_id


class ResultSpreadSheet:
    def __init__(self, id, scopes):
        self.id = id
        self.scopes = scopes
        self.sheet = None
        self.omraade_column = {} # { omraade_id : col_idx }
        self.parti_row = {} # { storkresid_parti_id : row_idx }
        self.person_row = {} # {omraade_parti_navn : row_idx}
        self.num_persons = {}

    def logon(self):
        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', self.scopes)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.service = build('sheets', 'v4', credentials=creds)

        # Call the Sheets API
        self.sheet = self.service.spreadsheets()

    def make_initial_setup(self, storkredse, opstillingskredse, afstemningsomraader):
        storkredse_sorted = {}

        opstillingskredse_sorted = {}

        for id in afstemningsomraader:
            if afstemningsomraader[id].parentID in opstillingskredse_sorted:
                opstillingskredse_sorted[afstemningsomraader[id].parentID].append(id)
            else:
                opstillingskredse_sorted[afstemningsomraader[id].parentID] = [id]

        for id in opstillingskredse:
            if opstillingskredse[id].parentID in storkredse_sorted:
                storkredse_sorted[opstillingskredse[id].parentID].append(id)
            else:
                storkredse_sorted[opstillingskredse[id].parentID] = [id]

        for storkreds_id in storkredse_sorted:
            self.num_persons[storkreds_id] = -1
            result = ['Total']
            self.omraade_column[storkreds_id] = get_column(2)
            counter = 3
            for opstillings_id in storkredse_sorted[storkreds_id]:
                self.omraade_column[opstillings_id] = get_column(counter)
                result.append(opstillingskredse[opstillings_id].navn)
                counter = counter+1
                for omraade in opstillingskredse_sorted[opstillings_id]:
                    self.omraade_column[omraade] = get_column(counter)
                    result.append(afstemningsomraader[omraade].navn)
                    counter = counter+1
            self.write_omraader(storkredse[storkreds_id].navn, result)
            print(storkredse[storkreds_id].navn + ": " + str(result))

    def write_omraader(self, storkreds_navn, omraader):

        body = {'values': [omraader]}
        r = storkreds_navn + '!B1:' + get_column(len(omraader)+1) + '1'
        try:
            self.service.spreadsheets().values().update(
                spreadsheetId=self.id,
                range= r,
                valueInputOption='USER_ENTERED',
                body=body).execute()
        except HttpError as error:
            print(error.content)
            exit(1)

    def update_omraade(self, omraade_resultat, alle_omraader, partier):
        storkreds_id = get_storkreds_id(omraade_resultat.omraadeID, alle_omraader)
        if storkreds_id is None:
            return

        if self.num_persons[storkreds_id] < 0:
            values = []
            persons = []
        else:
            values = range(self.num_persons[storkreds_id])
            persons = range(self.num_persons[storkreds_id])

        for parti_id in omraade_resultat.personResultat:
            if (storkreds_id+parti_id) not in self.parti_row:
                self.parti_row[storkreds_id + parti_id] = len(values)
                values.append([omraade_resultat.stemmer[parti_id]])
                persons.append([partier[parti_id].navn])
            else:
                values[self.parti_row[storkreds_id + parti_id]] = [omraade_resultat.stemmer[parti_id]]

            for person in omraade_resultat.personResultat[parti_id]:
                personID = storkreds_id + parti_id + person.navn
                if personID not in self.person_row:
                    self.person_row[personID] = len(values)
                    values.append([person.stemmer()])
                    persons.append([person.navn])
                else:
                    values[self.person_row[personID]] = [person.stemmer()]

        storkreds_navn = alle_omraader[storkreds_id].navn
        column = self.omraade_column[omraade_resultat.omraadeID]
        r = storkreds_navn + '!' + column + '2:' + column + str(len(values)+1)
        body = {'values': values}
        try:
            self.service.spreadsheets().values().update(
                spreadsheetId=self.id,
                range=r,
                valueInputOption='USER_ENTERED',
                body=body).execute()
        except HttpError as error:
            print('Values: ' + str(values))
            print(error.content)
            return False

        if self.num_persons[storkreds_id] < 0:
            r = storkreds_navn + '!A2:A' + str(len(values) + 2)
            self.num_persons[storkreds_id] = len(persons)
            body = {'values': persons}
            try:
                self.service.spreadsheets().values().update(
                    spreadsheetId=self.id,
                    range=r,
                    valueInputOption='USER_ENTERED',
                    body=body).execute()
            except HttpError as error:
                print('Persons: ' + str(persons))
                print(error.content)
                return False

        return True
